/*
The MIT License (MIT)

Copyright (c) 2015 José Bollo <jobol@nonadev.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#if !defined(_ECOAP_H_)
#define _ECOAP_H_
/********************* SECTION: eCoAP constants **************************/

/* known option values */
#define eCoAP_OPTION_If_Match                  1
#define eCoAP_OPTION_Uri_Host                  3
#define eCoAP_OPTION_ETag                      4
#define eCoAP_OPTION_If_None_Match             5
#define eCoAP_OPTION_Uri_Port                  7
#define eCoAP_OPTION_Location_Path             8
#define eCoAP_OPTION_Uri_Path                  11
#define eCoAP_OPTION_Content_Format            12
#define eCoAP_OPTION_Max_Age                   14
#define eCoAP_OPTION_Uri_Query                 15
#define eCoAP_OPTION_Accept                    17
#define eCoAP_OPTION_Location_Query            20
#define eCoAP_OPTION_Proxy_Uri                 35
#define eCoAP_OPTION_Proxy_Scheme              39
#define eCoAP_OPTION_Size1                     60

/* enumeration of the type of the options */
#define eCoAP_OPTIONTYPE_EMPTY                 0
#define eCoAP_OPTIONTYPE_UINT32                1
#define eCoAP_OPTIONTYPE_OPAQUE                2
#define eCoAP_OPTIONTYPE_STRING                3

/* code maker */
#define eCoAP_CODE(_class_,_detail_)           (((_class_) << eCoAP_CODE_CLASS_SHIFT) | (_detail_))

#define eCoAP_CODE_CLASS_SHIFT                 5
#define eCoAP_CODE_CLASS_MASK                  7

#define eCoAP_CODE_DETAIL_SHIFT                0
#define eCoAP_CODE_DETAIL_MASK                 31

/* code classes */
#define eCoAP_CODE_CLASS_REQUEST               0
#define eCoAP_CODE_CLASS_RESPONSE              2
#define eCoAP_CODE_CLASS_CLIENT_ERROR          4
#define eCoAP_CODE_CLASS_SERVER_ERROR          5

/* requests */
#define eCoAP_CODE_EMPTY                       eCoAP_CODE(eCoAP_CODE_CLASS_REQUEST, 0)
#define eCoAP_CODE_GET                         eCoAP_CODE(eCoAP_CODE_CLASS_REQUEST, 1)
#define eCoAP_CODE_POST                        eCoAP_CODE(eCoAP_CODE_CLASS_REQUEST, 2)
#define eCoAP_CODE_PUT                         eCoAP_CODE(eCoAP_CODE_CLASS_REQUEST, 3)
#define eCoAP_CODE_DELETE                      eCoAP_CODE(eCoAP_CODE_CLASS_REQUEST, 4)

/* responses */
#define eCoAP_CODE_Created                     eCoAP_CODE(eCoAP_CODE_CLASS_RESPONSE, 1)
#define eCoAP_CODE_Deleted                     eCoAP_CODE(eCoAP_CODE_CLASS_RESPONSE, 2)
#define eCoAP_CODE_Valid                       eCoAP_CODE(eCoAP_CODE_CLASS_RESPONSE, 3)
#define eCoAP_CODE_Changed                     eCoAP_CODE(eCoAP_CODE_CLASS_RESPONSE, 4)
#define eCoAP_CODE_Content                     eCoAP_CODE(eCoAP_CODE_CLASS_RESPONSE, 5)

/* client errors */
#define eCoAP_CODE_Bad_Request                 eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 0)
#define eCoAP_CODE_Unauthorized                eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 1)
#define eCoAP_CODE_Bad_Option                  eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 2)
#define eCoAP_CODE_Forbidden                   eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 3)
#define eCoAP_CODE_Not_Found                   eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 4)
#define eCoAP_CODE_Method_Not_Allowed          eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 5)
#define eCoAP_CODE_Not_Acceptable              eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 6)
#define eCoAP_CODE_Precondition_Failed         eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 12)
#define eCoAP_CODE_Request_Entity_Too_Large    eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 13)
#define eCoAP_CODE_Unsupported_Content_Format  eCoAP_CODE(eCoAP_CODE_CLASS_CLIENT_ERROR, 15)

/* interface errors */
#define eCoAP_CODE_Internal_Server_Error       eCoAP_CODE(eCoAP_CODE_CLASS_SERVER_ERROR, 0)
#define eCoAP_CODE_Not_Implemented             eCoAP_CODE(eCoAP_CODE_CLASS_SERVER_ERROR, 1)
#define eCoAP_CODE_Bad_Gateway                 eCoAP_CODE(eCoAP_CODE_CLASS_SERVER_ERROR, 2)
#define eCoAP_CODE_Service_Unavailable         eCoAP_CODE(eCoAP_CODE_CLASS_SERVER_ERROR, 3)
#define eCoAP_CODE_Gateway_Timeout             eCoAP_CODE(eCoAP_CODE_CLASS_SERVER_ERROR, 4)
#define eCoAP_CODE_Proxying_Not_Supported      eCoAP_CODE(eCoAP_CODE_CLASS_SERVER_ERROR, 5)

/* content formats */
#define eCoAP_FORMAT_Text_Plain                0
#define eCoAP_FORMAT_Application_Link_format   40
#define eCoAP_FORMAT_Application_Xml           41
#define eCoAP_FORMAT_Application_Octet_Stream  42
#define eCoAP_FORMAT_Application_Exi           47
#define eCoAP_FORMAT_Application_Json          50

/* ports */
#define eCoAP_DEFAULT_COAP_PORT                5683
#define eCoAP_DEFAULT_COAP_SERVICE             "5683"

#define eCoAP_DEFAULT_COAPS_PORT               5684
#define eCoAP_DEFAULT_COAPS_SERVICE            "5684"

/********************* SECTION: eCoAP functions **************************/

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>

/* **** events **** */

enum ecoap_event {
  ecoap_event_reset,
  ecoap_event_expired,
  ecoap_event_confirmed,
  ecoap_event_reply
};

/* **** setting **** */

extern void (*ecoap_out_of_memory)();

extern void (*ecoap_on_request)();
extern void (*ecoap_on_reply)(enum ecoap_event event, void *userdata);

extern void ecoap_trace(FILE *file, const char *desc);

/* **** interface **** */

extern int ecoap_interface_add(const struct sockaddr *addr, socklen_t addrlen);
extern int ecoap_interface_add_default(bool global, bool bound);

extern void ecoap_dispatch_events();

extern int ecoap_wait_max(int max_timeout);
extern int ecoap_wait();
extern int ecoap_init();

/* **** incoming **** */

extern bool ecoap_is_reading();

extern void ecoap_confirm();
extern void ecoap_reset();

extern void ecoap_treated();
extern void ecoap_forget();

extern void *ecoap_stash();
extern void ecoap_unstash(void *stash);

extern int ecoap_get_interface();
extern socklen_t ecoap_get_destination(const struct sockaddr **addr);

extern uint8_t ecoap_get_code();
extern uint8_t ecoap_get_class();
extern uint8_t ecoap_get_detail();

extern bool ecoap_is_request();
extern bool ecoap_is_response();
extern bool ecoap_is_error();
extern bool ecoap_is_client_error();
extern bool ecoap_is_server_error();
extern bool ecoap_is_get();
extern bool ecoap_is_post();
extern bool ecoap_is_put();
extern bool ecoap_is_delete();
extern bool ecoap_is_created();
extern bool ecoap_is_deleted();
extern bool ecoap_is_valid();
extern bool ecoap_is_changed();
extern bool ecoap_is_content();

/* **** read-data **** */

extern bool ecoap_at_option();
extern bool ecoap_at_content();
extern bool ecoap_at_data();
extern uint16_t ecoap_option_number();
extern uint16_t ecoap_option_index();

extern void ecoap_rewind();
extern void ecoap_next();

extern bool ecoap_select_option_at(uint16_t option, uint16_t index);
extern bool ecoap_select_option(uint16_t option);
extern bool ecoap_select_next_sibling();
extern bool ecoap_select_content();

extern bool ecoap_select_if_match();
extern bool ecoap_select_uri_host();
extern bool ecoap_select_e_tag();
extern bool ecoap_select_if_none_match();
extern bool ecoap_select_uri_port();
extern bool ecoap_select_location_path();
extern bool ecoap_select_uri_path();
extern bool ecoap_select_content_format();
extern bool ecoap_select_max_age();
extern bool ecoap_select_uri_query();
extern bool ecoap_select_accept();
extern bool ecoap_select_location_query();
extern bool ecoap_select_proxy_uri();
extern bool ecoap_select_proxy_scheme();
extern bool ecoap_select_size1();

struct ecoap_data {
  uint16_t length;
  const uint8_t *data;
};

extern uint16_t ecoap_get_length();
extern const void *ecoap_get_pointer();
extern struct ecoap_data ecoap_get_data();
extern uint32_t ecoap_get_uint32();
extern void *ecoap_get_copy();
extern char *ecoap_get_copyz();

extern int ecoap_compare(const char *text,  uint16_t length);
extern int ecoap_comparez(const char *text);

/* **** outgoing **** */

extern bool ecoap_is_writing();
extern void ecoap_cancel();

extern void ecoap_request(uint8_t code);
extern void ecoap_reply(uint8_t code);
extern void ecoap_request_get();
extern void ecoap_request_post();
extern void ecoap_request_put();
extern void ecoap_request_delete();
extern void ecoap_reply_created();
extern void ecoap_reply_deleted();
extern void ecoap_reply_valid();
extern void ecoap_reply_changed();
extern void ecoap_reply_content();
extern void ecoap_reply_bad_request();
extern void ecoap_reply_unauthorized();
extern void ecoap_reply_bad_option();
extern void ecoap_reply_forbidden();
extern void ecoap_reply_not_found();
extern void ecoap_reply_method_not_allowed();
extern void ecoap_reply_not_acceptable();
extern void ecoap_reply_precondition_failed();
extern void ecoap_reply_request_entity_too_large();
extern void ecoap_reply_unsupported_content_format();
extern void ecoap_reply_internal_server_error();
extern void ecoap_reply_not_implemented();
extern void ecoap_reply_bad_gateway();
extern void ecoap_reply_service_unavailable();
extern void ecoap_reply_gateway_timeout();
extern void ecoap_reply_proxying_not_supported();

extern void ecoap_put_interface(int itfid);
extern void ecoap_put_default_interface(int itfid);
extern void ecoap_put_destination(const struct sockaddr *addr, socklen_t addrlen);
extern void ecoap_put_userdata(void *userdata);
extern void ecoap_put_confirmable(bool confirm);
extern void ecoap_put_handler(void (*handler)(enum ecoap_event event, void *userdata));

extern bool ecoap_has_put_option(uint16_t option);
extern void ecoap_put_option_opaque(const void *buffer, uint16_t length, uint16_t option);
extern void ecoap_put_option_string(const char *string, uint16_t length, uint16_t option);
extern void ecoap_put_option_stringz(const char *string, uint16_t option);
extern void ecoap_put_option_encoded(const char *string, uint16_t length, uint16_t option);
extern void ecoap_put_option_encodedz(const char *string, uint16_t option);
extern void ecoap_put_option_empty(uint16_t option);
extern void ecoap_put_option_uint32(uint32_t value, uint16_t option);

extern void ecoap_put_if_match(const void *value, uint16_t length);
extern void ecoap_put_if_matchz(const char *value);
extern void ecoap_put_uri_host(const char *value, uint16_t length);
extern void ecoap_put_uri_hostz(const char *value);
extern void ecoap_put_e_tag(const void *value, uint16_t length);
extern void ecoap_put_e_tagz(const char *value);
extern void ecoap_put_if_none_match();
extern void ecoap_put_uri_port(uint16_t value);
extern void ecoap_put_location_path(const char *value, uint16_t length);
extern void ecoap_put_location_pathz(const char *value);
extern void ecoap_put_uri_path(const char *value, uint16_t length);
extern void ecoap_put_uri_pathz(const char *value);
extern void ecoap_put_content_format(uint16_t value);
extern void ecoap_put_max_age(uint32_t value);
extern void ecoap_put_uri_query(const char *value, uint16_t length);
extern void ecoap_put_uri_queryz(const char *value);
extern void ecoap_put_accept(uint16_t value);
extern void ecoap_put_location_query(const char *value, uint16_t length);
extern void ecoap_put_location_queryz(const char *value);
extern void ecoap_put_proxy_uri(const char *value, uint16_t length);
extern void ecoap_put_proxy_uriz(const char *value);
extern void ecoap_put_proxy_scheme(const char *value, uint16_t length);
extern void ecoap_put_proxy_schemez(const char *value);
extern void ecoap_put_size1(uint32_t value);
extern void ecoap_put_content(const void *data, uint16_t length);
extern void ecoap_put_contentz(const char *value);
extern int ecoap_put_uri(const char *uri);

extern int ecoap_send();

/* **** option **** */

struct ecoap_option {
  const char* name;      /* the name of the option */
  uint16_t id;           /* id for the protocole */
  bool repeatable;       /* is the option repeatable? */
  uint8_t type;          /* the type of the option */
  int mini;              /* minimal defined value */
  int maxi;              /* maximal defined value */
};

extern const struct ecoap_option *ecoap_option_by_number(uint16_t option);
extern const struct ecoap_option *ecoap_option_by_name(const char *name);

/* **** CoRE **** */

extern void ecoap_core_dispatch();

extern bool ecoap_core_path(const char *path);

extern void ecoap_core_on(uint8_t code, void (*handler)(void *userdata));
extern void ecoap_core_on_get(void (*handler)(void *userdata));
extern void ecoap_core_on_post(void (*handler)(void *userdata));
extern void ecoap_core_on_put(void (*handler)(void *userdata));
extern void ecoap_core_on_delete(void (*handler)(void *userdata));

extern void ecoap_core_userdata(void *userdata);

extern void ecoap_core_attr(const char *value, const char *name, bool quote);
extern void ecoap_core_anchor(const char *value);
extern void ecoap_core_ct(const char *value);
extern void ecoap_core_hreflang(const char *value);
extern void ecoap_core_if(const char *value);
extern void ecoap_core_media(const char *value);
extern void ecoap_core_rel(const char *value);
extern void ecoap_core_rev(const char *value);
extern void ecoap_core_rt(const char *value);
extern void ecoap_core_sz(const char *value);
extern void ecoap_core_title(const char *value);
extern void ecoap_core_title_star(const char *value);
extern void ecoap_core_type(const char *value);

/********************************* END ***********************************/
#endif
/* vim: set sts=2 sta sw=2 et ts=8 wrap: */ 
