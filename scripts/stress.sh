#!/bin/bash

ip=${1:-127.0.0.1}
st=${2:-.25}
cnt=${3:-100}

echo "stressing $ip: ${st}s / $cnt"

{
while :; do
  x=$RANDOM
  case $x in
  1*) echo "uri coap://$ip/jo?$x send";;
  9*) echo "uri coap://$ip/ignore?$x send";;
  *) echo "uri coap://$ip/reset?$x send";;
  esac
  if [ -n "$cnt" ]; then
    if [ "$cnt" -le 1 ]; then
      sleep 5
      break
    fi
    cnt=$(expr $cnt - 1)
  fi
  sleep $st
done
sleep 3600
} |
./test-cli

