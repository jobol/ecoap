/*
The MIT License (MIT)

Copyright (c) 2015 José Bollo <jobol@nonadev.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "ecoap.h"

static const char indexing[] =
  "Test server made with ecoap (see http://gitlab.com/jobol/ecoap)\n" \
  "Copyright (C) 2015 José Bollo <jobol@nonadev.net>\n"
  "\n";

static bool running;
static struct timespec origin;

void get_constant_string(void *userdata)
{
  ecoap_reply_content();
  ecoap_put_content_format(eCoAP_FORMAT_Text_Plain);
  ecoap_put_max_age(0x2ffff);
  ecoap_put_contentz((const char*)userdata);
  ecoap_send();
  ecoap_forget();
}

void time_get()
{
  char buf[40];
  struct timespec t;
  int n;

  if (!running) {
    ecoap_reply_not_found();
    ecoap_forget();
  } else {
    clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    t.tv_sec -= origin.tv_sec;
    t.tv_nsec -= origin.tv_nsec;
    if (t.tv_nsec < 0) {
      t.tv_nsec += 1000000000;
      t.tv_sec--;
    }
    if (ecoap_select_uri_query() && ecoap_comparez("ticks"))
      n = snprintf(buf, sizeof buf, "%llu", (long long unsigned)t.tv_sec);
    else
      n = snprintf(buf, sizeof buf, "%d:%d:%d", (int)(t.tv_sec/3600), (int)((t.tv_sec/60)%60), (int)(t.tv_sec%60));
    ecoap_reply_content();
    ecoap_put_content_format(eCoAP_FORMAT_Text_Plain);
    ecoap_put_max_age(1);
    ecoap_put_content(buf, (uint16_t)n);
  }
  ecoap_send();
}

void time_put()
{
  struct ecoap_data d;

  if (running)
    ecoap_reply_changed();
  else
    ecoap_reply_created();
  ecoap_send();

  running = true;
  clock_gettime(CLOCK_MONOTONIC_RAW, &origin);

  if (ecoap_select_content()) {
    d = ecoap_get_data();
    if (d.length != 0) 
      origin.tv_sec -= (time_t)atoll((const char *)d.data);
  }
}

void time_delete()
{
  running = false;
  ecoap_reply_deleted();
  ecoap_send();
}



/********************************* END ***********************************/


void init_resources()
{
  ecoap_core_path("/");
  ecoap_core_ct("0");
  ecoap_core_title("General Info");
  ecoap_core_userdata((void*)indexing);
  ecoap_core_on_get(get_constant_string);

  ecoap_core_path("/time");
  ecoap_core_ct("0");
  ecoap_core_title("Internal Clock");
  ecoap_core_rt("Ticks");
  ecoap_core_if("clock");
  ecoap_core_on_get(time_get);
  ecoap_core_on_put(time_put);
  ecoap_core_on_delete(time_delete);
}

int main(int argc, char **argv) {
  ecoap_trace(stdout, "*");
  ecoap_init();
  ecoap_on_request = ecoap_core_dispatch;
  ecoap_interface_add_default(true, true);
  init_resources();
  running = true;
  clock_gettime(CLOCK_MONOTONIC_RAW, &origin);

  for (;;) {
    ecoap_wait_max(5);
    ecoap_dispatch_events();
  }
}
