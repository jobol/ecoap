
#include <stdio.h>
#include <poll.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <ecoap.h>

#if defined(CLIENT)
#  define CS(c,s) (c)
#elif defined(SERVER)
#  define CS(c,s) (s)
#else
#  error "CLIENT OR SERVER MUST BE DEFINED!"
#endif

void onlinein(char *line)
{
  int opt = 0;
  const struct ecoap_option *optdsc;
  char *tok;
  static enum {
    _nothing,
    _started,
    _option,
    _value,
    _uri
  }
    state = _nothing;

  tok = strtok(line, " \t\n\r");
  while (tok != NULL) {
    switch(state) {
    case _nothing:
      if (!strcmp(tok, "get"))
      	ecoap_request_get();
      else if (!strcmp(tok, "post"))
      	ecoap_request_post();
      else if (!strcmp(tok, "put"))
      	ecoap_request_put();
      else if (!strcmp(tok, "delete"))
      	ecoap_request_delete();
      else {
      	ecoap_request_get();
      	ecoap_put_confirmable(true);
      	state = _started;
        continue;
      }
      ecoap_put_confirmable(true);
      state = _started;
      break;
    case _started:
      if (!strcmp(tok, "send")) {
        ecoap_send();
        state = _nothing;
        break;
      }
      if (!strcmp(tok, "option")) {
        state = _option;
        break;
      }
      if (!strcmp(tok, "uri")) {
        state = _uri;
        break;
      }
      ecoap_put_content(tok, strlen(tok));
      break;
    case _option:
      optdsc = ecoap_option_by_name(tok);
      opt = optdsc==NULL ? atoi(tok) : optdsc->id;
      state = _value;
      break;
    case _value:
      switch (tok[0]) {
      case '#':
        ecoap_put_option_uint32((uint32_t)atoi(tok+1), (uint16_t)opt);
        break;
      case '@':
        ecoap_put_option_encodedz(tok+1, (uint16_t)opt);
        break;
      default:
        ecoap_put_option_stringz(tok, (uint16_t)opt);
        break;
      }
      state = _started;
      break;
    case _uri:
      opt = ecoap_put_uri(tok);
      if (opt != 0)
        printf("error %d returned by set-uri for %s\n", opt, tok);
      state = _started;
      break;
    }
    tok = strtok(NULL, " \t\n\r");
  }
}

void oninput()
{
  static char *line = NULL;
  static size_t pos = 0;
  char c;
  /* sub optimal... */
  while(read(0, &c, 1) == 1) {
    line = realloc(line, pos + 1);
    if (c == '\n') {
      line[pos] = 0;
      onlinein(line);
      pos = 0;
    } else {
      line[pos++] = c;
    }
  }
}

void dumpincoming()
{
  ecoap_rewind();
  while (ecoap_at_option()) {
    uint16_t num = ecoap_option_number();
    uint16_t idx = ecoap_option_index();
    const struct ecoap_option *dsc = ecoap_option_by_number(num);
    struct ecoap_data data = ecoap_get_data();
    if (dsc == NULL)
      printf("option %d.%d length: %d\n", (int)num, (int)idx, (int)data.length);
    else
      switch (dsc->type) {
      case eCoAP_OPTIONTYPE_EMPTY:
      case eCoAP_OPTIONTYPE_OPAQUE:
        printf("option %s.%d length: %d\n", dsc->name, (int)idx, (int)data.length);
        break;
      case eCoAP_OPTIONTYPE_UINT32:
        printf("option %s.%d value: %d\n", dsc->name, (int)idx, (int)ecoap_get_uint32());
        break;
      case eCoAP_OPTIONTYPE_STRING:
        printf("option %s.%d value: %.*s\n", dsc->name, (int)idx, (int)data.length, (const char*)data.data);
        break;
      }
    ecoap_next();
  }
  if (ecoap_at_content()) {
    struct ecoap_data data = ecoap_get_data();
    if (data.length)
      printf("content length: %d\n", data.length);
  }
  fflush(stdout);
}

void onrequest()
{
  printf("\n<onrequest>\n");
  dumpincoming();
  if (!ecoap_select_uri_path())
    ecoap_reset();
  else if (0 == ecoap_comparez("reset"))
    ecoap_reset();
  else if (0 == ecoap_comparez("ignore"))
    ;
  else {
    ecoap_reply(eCoAP_CODE_Content);
    if (0 == ecoap_comparez("case")) {
      if (!ecoap_select_next_sibling()) {
        ecoap_cancel();
        ecoap_reset();
        return;
      }
      if (0 == ecoap_comparez("0"))
        ecoap_put_contentz("1");
      else if (0 == ecoap_comparez("1"))
        ecoap_put_contentz("2");
      else
        ecoap_put_contentz("0");
    }
    ecoap_send();
  }
}

void onreply(void *userdata)
{
  printf("\n<onreply>\n");
  dumpincoming();
  ecoap_confirm();
}

void onconfirmed(void *userdata)
{
  printf("\n<onconfirmed>\n");
}

void onreset(void *userdata)
{
  printf("\n<onreset>\n");
}

void onexpired(void *userdata)
{
  printf("\n<onexpired>\n");
}

void replyhandler(enum ecoap_event event, void *userdata)
{
  switch(event) {
  case ecoap_event_reply: onreply(userdata); break;
  case ecoap_event_confirmed: onconfirmed(userdata); break;
  case ecoap_event_reset: onreset(userdata); break;
  case ecoap_event_expired: onexpired(userdata); break;
  }
}

void onecoap()
{
  int sts = ecoap_wait_max(0);
  if (sts == 0)
    ecoap_dispatch_events();
}

int main(int argc, char**argv)
{
  struct pollfd polls[2];
  int fd;

  ecoap_trace(stdout, "*");
  fd = ecoap_init();
  ecoap_interface_add_default(true, CS(false,true));
  ecoap_on_request = onrequest;
  ecoap_on_reply = replyhandler;

  fcntl(0, F_SETFL, O_NONBLOCK);
  polls[0].fd = 0;
  polls[0].events = POLLIN;
  polls[1].fd = fd;
  polls[1].events = POLLIN;

  for(;;) {
    if (poll(polls, 2, -1) < 0)
      break;
    if ((polls[0].revents|polls[1].revents) & ~POLLIN)
      break;
    if (polls[0].revents & POLLIN)
      oninput();
    if (polls[1].revents & POLLIN)
      onecoap();
  }
  return 0;
}


