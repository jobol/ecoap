.PHONY: all
all: lib examples

.PHONY: clean-all
clean-all: clean-lib clean-examples

CFLAGS = -Wall -I. -fPIC
CFLAGS += -g
#CFLAGS += -DNDEBUG
CFLAGS += -pthread
CFLAGS += -O3
CFLAGS += -fno-inline-functions
CFLAGS += -finline-functions-called-once
CFLAGS += -finline-small-functions
CFLAGS += -ffunction-sections
CFLAGS += -fdata-sections
CFLAGS += -Wl,--gc-sections
CFLAGS += -L. 
LDFLAGS += --gc-sections
LDFLAGS += -L. 
LDLIBS += -l:libecoap.a

lib: libecoap.a

.PHONY: clean-lib clean
clean-lib clean:
	rm -f *.o *.a

libecoap.a: ecoap.o
	ar r libecoap.a ecoap.o
	ld -shared ecoap.o -E -o libecoap.so

ecoap.o: ecoap.c ecoap.h

.PHONY: examples clean-examples

examples: examples/tiny examples/server test-cli test-srv

clean-examples:
	rm examples/tiny examples/server test-cli test-srv

test-cli: examples/test-cs.c libecoap.a
	$(CC) $(CFLAGS) -DCLIENT -o $@ $< $(LDLIBS)

test-srv: examples/test-cs.c libecoap.a
	$(CC) $(CFLAGS) -DSERVER -o $@ $< $(LDLIBS)

examples/tiny: examples/tiny.c libecoap.a ecoap.h
	$(CC) $(CFLAGS) -o $@ $< $(LDLIBS)

examples/server: examples/server.c libecoap.a ecoap.h
	$(CC) $(CFLAGS) -o $@ $< $(LDLIBS)
