#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>


const char *familyname(int num)
{
  static char *names[] = {
    /*  0..4  */ "AF_UNSPEC", "AF_LOCAL", "AF_INET", "AF_AX25", "AF_IPX",
    /*  5..9  */ "AF_APPLETALK", "AF_NETROM", "AF_BRIDGE", "AF_ATMPVC", "AF_X25",
    /* 10..14 */ "AF_INET6", "AF_ROSE", "AF_DEC", "AF_NETBEUI", "AF_SECURITY",
    /* 15..19 */ "AF_KEY", "AF_NETLINK", "AF_PACKET", "AF_ASH", "AF_ECONET",
    /* 20..24 */ "AF_ATMSVC", "AF_RDS", "AF_SNA", "AF_IRDA", "AF_PPPOX",
    /* 25..29 */ "AF_WANPIPE", "AF_LLC", NULL, NULL, "AF_CAN",
    /* 30..34 */ "AF_TIPC", "AF_BLUETOOTH", "AF_IUCV", "AF_RXRPC", "AF_ISDN",
    /* 35..39 */ "AF_PHONET", "AF_IEEE802154", "AF_CAIF", "AF_ALG", "AF_NFC",
    /* 40 */     "AF_VSOCK"
  };
  static char buffer[30];

  char * result = NULL;

  if (0 <= num && num < sizeof names / sizeof * names)
    result = names[num];
  if (result == NULL) {
    sprintf(buffer,"%d?",num);
    result = buffer;
  }
  return result;
}

const char *socktypename(int num)
{
  static char *names[] = {
    /* 0 .. 4 */ NULL, "SOCK_STREAM", "SOCK_DGRAM", "SOCK_RAW", "SOCK_RDM",
    /* 5 .. 9 */ "SOCK_SEQPACKET", "SOCK_DCCP", NULL, NULL, NULL,
    /* 10 */     "SOCK_PACKET"
  };
  static char buffer[30];

  char * result = NULL;

  if (0 <= num && num < sizeof names / sizeof * names)
    result = names[num];
  if (result == NULL) {
    sprintf(buffer,"%d?",num);
    result = buffer;
  }
  return result;
}

const char *protoname(int num)
{
  static char buffer[30];
  struct protoent *p = getprotobynumber(num);
  if (p != NULL)
    return p->p_name;
  sprintf(buffer,"%d?",num);
  return buffer;
}

int main(int ac, char **av)
{
  int s;
  char *z = "http";
  struct addrinfo *r, *i, h = {
    .ai_flags      = AI_PASSIVE,
    .ai_family     = AF_UNSPEC,
    .ai_socktype   = 0,
    .ai_protocol   = 0,
    .ai_addrlen    = 0,
    .ai_addr       = NULL,
    .ai_canonname  = NULL,
    .ai_next       = NULL };

  av++;
  while(*av) {
    if(!strcmp(*av,"-6")) {
      h.ai_family = AF_INET6;
    } else if(!strcmp(*av,"-4")) {
      h.ai_family = AF_INET;
    } else if(!strcmp(*av,"-u")) {
      h.ai_socktype = SOCK_DGRAM;
    } else if(!strcmp(*av,"-t")) {
      h.ai_socktype = SOCK_STREAM;
    } else if(!strcmp(*av,"-s")) {
      if (av[1]) {
        z = *++av;
        if (!strcmp(z,"none"))
          z = NULL;
      }
    } else {
      printf("Looking at '%s'...",*av);
      if (!strcmp(*av,"-")) {
        h.ai_flags = AI_PASSIVE;
        s = getaddrinfo(NULL, z, &h, &r);
      } else {
        h.ai_flags = AI_CANONNAME;
        s = getaddrinfo(*av, z, &h, &r);
      }
      if (s != 0) {
        printf(" ERROR %s", gai_strerror(s));
      } else {
        printf(" %s\n",r->ai_canonname);
        for(i=r ; i != NULL ; i = i->ai_next) {
          printf("   f=%s t=%s P=%s ",
            familyname(i->ai_family),
            socktypename(i->ai_socktype),
            protoname(i->ai_protocol)
            );
          if (i->ai_addr->sa_family == AF_INET) {
            struct sockaddr_in *a = (struct sockaddr_in*)(i->ai_addr);
            unsigned char *ipv4 = (unsigned char*)&(a->sin_addr.s_addr);
            unsigned char *port = (unsigned char*)&(a->sin_port);
            printf("a=%d.%d.%d.%d p=%d",
              (int)ipv4[0],
              (int)ipv4[1],
              (int)ipv4[2],
              (int)ipv4[3],
              (((int)port[0]) << 8)|(int)port[1]);
          } else if (i->ai_addr->sa_family == AF_INET6) {
            struct sockaddr_in6 *a = (struct sockaddr_in6*)(i->ai_addr);
            unsigned char *ipv6 = a->sin6_addr.s6_addr;
            unsigned char *port = (unsigned char*)&(a->sin6_port);
            printf("a=%x:%x:%x:%x:%x:%x:%x:%x p=%d",
              (((int)ipv6[0]) << 8)|(int)ipv6[1],
              (((int)ipv6[2]) << 8)|(int)ipv6[3],
              (((int)ipv6[4]) << 8)|(int)ipv6[5],
              (((int)ipv6[6]) << 8)|(int)ipv6[7],
              (((int)ipv6[8]) << 8)|(int)ipv6[9],
              (((int)ipv6[10]) << 8)|(int)ipv6[11],
              (((int)ipv6[12]) << 8)|(int)ipv6[13],
              (((int)ipv6[14]) << 8)|(int)ipv6[15],
              (((int)port[0]) << 8)|(int)port[1]);
          } else {
            printf("?");
          }
          printf("\n");
        }
        freeaddrinfo(r);
      }
      printf("\n");
    }
    av++;
  }
  return 0;
}

