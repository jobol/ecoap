
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <mcheck.h>

#include <ecoap.h>

void process()
{
  ecoap_send();
  ecoap_wait();
}

int main(int argc, char**argv)
{
  char *line = NULL;
  size_t size = 0;
  ssize_t ssz;
  int opt = 0;
  const struct ecoap_option *optdsc;
  char *tok;
  enum {
    _nothing,
    _started,
    _option,
    _value,
    _uri
  }
    state = _nothing;

  mtrace();
  ecoap_trace(stdout, "*");
  ecoap_init();
  ecoap_interface_add_default(false, true);
  while ((ssz = getline(&line, &size, stdin)) > 0) {
    tok = strtok(line, " \t\n\r");
    while (tok != NULL) {
      switch(state) {
      case _nothing:
        ecoap_request_get();
        ecoap_put_confirmable(true);
        state = _started;
      case _started:
        if (!strcmp(tok, "send")) {
          process();
          state = _nothing;
          break;
        }
        if (!strcmp(tok, "option")) {
          state = _option;
          break;
        }
        if (!strcmp(tok, "uri")) {
          state = _uri;
          break;
        }
        ecoap_put_content(tok, strlen(tok));
        break;
      case _option:
        optdsc = ecoap_option_by_name(tok);
        opt = optdsc==NULL ? atoi(tok) : optdsc->id;
        state = _value;
        break;
      case _value:
        switch (tok[0]) {
        case '#':
          ecoap_put_option_uint32((uint32_t)atoi(tok+1), (uint16_t)opt);
          break;
        case '@':
          ecoap_put_option_encodedz(tok+1, (uint16_t)opt);
          break;
        default:
          ecoap_put_option_stringz(tok, (uint16_t)opt);
          break;
        }
        state = _started;
        break;
      case _uri:
        opt = ecoap_put_uri(tok);
	if (opt != 0)
	  printf("error %d returned by set-uri for %s\n", opt, tok);
        state = _started;
        break;
      }
      tok = strtok(NULL, " \t\n\r");
    }
  }
  if (state != _nothing)
    process();
  return 0;
}


