
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>


pthread_mutex_t lres = PTHREAD_MUTEX_INITIALIZER;
int resset = 0;
uint16_t resfact;
uint16_t resincr;
uint32_t coal[5];

void setres(uint16_t f, uint16_t i, uint32_t c[5])
{
  int j;
  pthread_mutex_lock(&lres);
  if (resset) {
    for (j=0 ; j < 5 ; j++) {
      if (coal[j] > c[j]) {
        pthread_mutex_unlock(&lres);
	return;
      }
      if (coal[j] < c[j]) break;
    }
  }
  resfact = f;
  resincr = i;
  for (j = 0 ; j < 5 ; j++) coal[j] = c[j];
  resset = 1;
  printf("set f=%d i=%d", (int)f, (int)i);
  for (j = 0 ; j < 5 ; j++) printf(" %d",coal[j]);
  printf("\n");
  fflush(stdout);
  pthread_mutex_unlock(&lres);
}

void scanres(uint16_t f, uint16_t i, uint16_t v[256], uint16_t dv[256])
{
  int j;
  int32_t c[5], x;
  c[0] = c[1] = c[3] = v[0];
  c[2] = c[4] = dv[0];
  for (j = 1 ; j < 256 ; j++) {
    x = v[j];
    c[0] += x;
    if (x < c[1]) c[1] = x; else if (x > c[3]) c[3] = x;
    x = dv[j];
    if (x < c[2]) c[2] = x; else if (x > c[4]) c[4] = x;
  }
  c[3] = -c[3];
  c[4] = -c[4];
  setres(f, i, c);
}

void test(uint16_t f, uint16_t i)
{
  uint8_t j, z[65536];
  uint16_t m[2][256];
  uint16_t pv = 1, v, dv;
  memset(m, 0, sizeof m);
  memset(z, 0, sizeof z);
  v = pv * f + i;
  while(!z[v]) {
    z[v] = 1;
    dv = pv ^ v;
    pv = v;
    if (!++m[0][(v >> 8) & 255]) return;
    if (!++m[1][(dv >> 8) & 255]) return;
    v = pv * f + i;
  }
  scanres(f, i, m[0], m[1]);
}

pthread_mutex_t lpar = PTHREAD_MUTEX_INITIALIZER;

uint16_t factor = 1;
uint16_t incr = 1;


void *run(void*a)
{
  uint16_t f, i;
  for(;;) {
    pthread_mutex_lock(&lpar);
    f = factor;
    i = incr + 2;
    if (i < incr) {
      f += 2;
      if (f < factor) {
        pthread_mutex_unlock(&lpar);
	break;
      }
      if (f%50==1) { printf("scans f %d..%d\n",f,f+48); fflush(stdout); }
      factor = f;
      i = 1;
    }
    incr = i;
    pthread_mutex_unlock(&lpar);
    test(f, i);
  }
  return 0;
}

int main(int ac, char **av)
{
  pthread_t p[8];
  int i;
factor = 47799; incr -= 2; resset = 1;
coal[0] = 65536; coal[1] = 256; coal[2] = 252; coal[3] = -256; coal[4] = -260;
//  test(3,0);return 0;
  for (i=0 ; i < 8 ; i++) pthread_create(p+i, 0, run, 0);
  for (i=0 ; i < 8 ; i++) pthread_join(p[i], NULL);
  return 0;
}

